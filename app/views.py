from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination

from rest_framework import filters
from rest_framework.filters import SearchFilter
from django_filters import rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend


from .custom_backend import CustomBackend

from .models import User
from .models import Product
from .models import Sale
from .models import Production
from .models import Request

from .serializers import UserSerializer
from .serializers import ProductSerializer
from .serializers import SaleSerializer
from .serializers import ProductionSerializer
from .serializers import RequestSerializer


@api_view(["POST"])
def sign_in(request):
    email = request.data.get("email")
    password = request.data.get("password")

    if not (email and password):
        return Response(
            data={"detail": "Need dictionary {email:<email>, password:<password>}"},
            status=400,
        )
    custom_backend = CustomBackend()
    user = custom_backend.authenticate(email=email, password=password)
    if not user:
        return Response(data={"detail": "Email or password incorrect"}, status=401)

    return Response(data={"authorization_token": user.auth_token.key, "id": user.id}, status=200)


@api_view(["POST"])
def sign_up(request):
    """
           Create user.

           Request and response examples:
          ```
          Response value:
            {
                "email": "admin@mail.ru",
                "password": "admin",
                "first_name": "Антуан",
                "last_name": "Хуан",
                "patronymic": "Олегович"
            }

    """

    first_name = request.data.get("first_name")
    last_name = request.data.get("last_name")
    password = request.data.get("password")
    patronymic = request.data.get("patronymic")
    email = request.data.get("email")

    if not (first_name and last_name and password and email and patronymic):
        return Response(
            data={
                "detail": "Need dictionary {first_name:<first_name>, last_name:<last_name>, patronymic: <patronymic>, "
                          "email:<email>, password:<password>} "
            },
            status=406,
        )

    if User.objects.filter(email=email).exists():
        return Response(data={"detail": "This user already exists"}, status=400)

    email_validator = EmailValidator()
    try:
        email_validator(email)
    except ValidationError:
        return Response(
            data={"detail": "{} has validation error".format(email)}, status=406
        )
    new_user = User.objects.create_user(
        email=email,
        password=password,
        first_name=first_name,
        last_name=last_name,
        patronymic=patronymic
    )
    return Response(data={"authorization_token": new_user.auth_token.key, "id": new_user.id}, status=201)


class UserListView(ListCreateAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = User.objects.filter(is_active=True)
    serializer_class = UserSerializer


class UserDetailView(RetrieveUpdateDestroyAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = User.objects.filter(is_active=True)
    serializer_class = UserSerializer


class ProductFilter(filters.FilterSet):

    class Meta:
        model = Product
        fields = ['name', 'serial_number']


class ProductListView(ListCreateAPIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    search_fields = ['name', 'serial_number']
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filterset_class = ProductFilter
    pagination_class = PageNumberPagination


class ProductDetailVIew(RetrieveUpdateDestroyAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class SaleFilter(filters.FilterSet):
    sale_date = filters.RangeFilter()
    quantity = filters.RangeFilter()

    class Meta:
        model = Sale
        fields = ['sale_date', 'quantity', 'name', 'serial_number']


class SaleListView(ListCreateAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer
    search_fields = ['name', 'serial_number']
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filterset_class = SaleFilter
    pagination_class = PageNumberPagination


class SaleDetailVIew(RetrieveUpdateDestroyAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer


class ProductionFilter(filters.FilterSet):
    production_date = filters.RangeFilter()
    quantity = filters.RangeFilter()

    class Meta:
        model = Production
        fields = ['production_date', 'quantity', 'name', 'serial_number']


class ProductionListView(ListCreateAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Production.objects.all()
    serializer_class = ProductionSerializer
    search_fields = ['name', 'serial_number']
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filterset_class = ProductionFilter


class ProductionDetailView(RetrieveUpdateDestroyAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Production.objects.all()
    serializer_class = ProductionSerializer


class RequestListView(ListCreateAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Request.objects.all()
    serializer_class = RequestSerializer


class RequestDetailVIew(RetrieveUpdateDestroyAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = Request.objects.all()
    serializer_class = RequestSerializer



