from django.contrib import admin
from .models import User, Product, Production, Sale, Request


admin.site.register(Product)
admin.site.register(User)
admin.site.register(Production)
admin.site.register(Sale)
admin.site.register(Request)

