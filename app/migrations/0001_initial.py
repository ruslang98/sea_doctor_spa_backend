# Generated by Django 3.0.5 on 2020-07-01 17:28

import app.models
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('email', models.EmailField(max_length=254, unique=True, verbose_name='E-mail')),
                ('region', models.CharField(max_length=50, verbose_name='Регион')),
                ('city', models.CharField(blank=True, max_length=30, null=True, verbose_name='Город')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='Имя')),
                ('last_name', models.CharField(blank=True, max_length=30, verbose_name='Фамилия')),
                ('patronymic', models.CharField(blank=True, max_length=30, verbose_name='Отчество')),
                ('birthday', models.DateField(verbose_name='Дата рождения')),
                ('phone', models.IntegerField(verbose_name='Номер телефона')),
                ('skype', models.CharField(max_length=20, verbose_name='Skype')),
                ('date_joined', models.DateTimeField(auto_now_add=True)),
                ('is_active', models.BooleanField(default=True)),
                ('is_confirm', models.BooleanField(default=False)),
                ('is_staff', models.BooleanField(default=True)),
                ('avatar', models.ImageField(blank=True, null=True, upload_to=app.models.get_avatar_path)),
                ('position', models.CharField(choices=[('ZO', 'Zero'), ('LS', 'Logistics'), ('TL', 'Technical')], default='TL', max_length=30, verbose_name='Должность')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'Пользователь',
                'verbose_name_plural': 'Пользователи',
            },
            managers=[
                ('objects', app.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('serial_number', models.CharField(max_length=30, verbose_name='Серийный номер')),
                ('name', models.TextField(verbose_name='Наименование')),
                ('ingredients', models.TextField(verbose_name='Ингредиенты')),
                ('contraindications', models.TextField(verbose_name='Противопоказания')),
                ('expiration_date', models.CharField(max_length=30, verbose_name='Срок годносии')),
            ],
        ),
        migrations.CreateModel(
            name='Sale',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('serial_number', models.CharField(max_length=30, verbose_name='Серийный номер')),
                ('name', models.TextField(verbose_name='Наименование')),
                ('sale_date', models.DateField()),
                ('quantity', models.IntegerField(verbose_name='Количество')),
                ('price', models.IntegerField(verbose_name='Цена за ед.')),
                ('sum', models.IntegerField(verbose_name='Сумма')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Польователь')),
            ],
        ),
        migrations.CreateModel(
            name='Production',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('serial_number', models.CharField(max_length=30, verbose_name='Серийный номер')),
                ('name', models.TextField(verbose_name='Наименование')),
                ('ingredients', models.TextField(verbose_name='Ингредиенты')),
                ('quantity', models.IntegerField(verbose_name='Количество')),
                ('production_date', models.DateField()),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Польователь')),
            ],
        ),
        migrations.CreateModel(
            name='Help',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(verbose_name='Текст')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
        ),
    ]
