from rest_framework import serializers

from .models import User, Product, Sale, Production, Request


class UserSerializer(serializers.ModelSerializer):
    date_joined = serializers.DateTimeField(format="%Y.%m.%d %H:%M:%S")

    class Meta:
        model = User
        fields = [
            "id",
            "first_name",
            "last_name",
            "patronymic",
            "region",
            "city",
            "birthday",
            "phone",
            "skype",
            "date_joined",
            "avatar",
            "position",
        ]


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = [
            "id",
            "serial_number",
            "name",
            "ingredients",
            "contraindications",
            "valid_from",
            "valid_to",
        ]


class SaleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sale
        fields = [
            "id",
            "user",
            "serial_number",
            "name",
            "sale_date",
            "quantity",
            "price",
            "sum",
        ]


class ProductionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Production
        fields = [
            "id",
            "user",
            "serial_number",
            "name",
            "ingredients",
            "quantity",
            "production_date",
        ]


class RequestSerializer(serializers.ModelSerializer):

    class Meta:
        model = Request
        fields = [
            "id",
            "user",
            "text",
        ]

