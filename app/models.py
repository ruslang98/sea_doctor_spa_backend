from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.base_user import BaseUserManager

from rest_framework.authtoken.models import Token


def get_avatar_path(instance, filename):
    return "avatars/{}/{}".format(instance.email, filename)


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        Token.objects.create(user=user)
        return user

    def create_user(self, email, password=None, **extra_fields):

        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    PRODUCT_MANAGER = 'ZO'
    SALES_MANAGER = 'LS'
    DIRECTOR = 'TL'

    DEPARTAMENT_CHOICES = [
        (PRODUCT_MANAGER, 'Zero'),
        (SALES_MANAGER, 'Logistics'),
        (DIRECTOR, 'Technical'),
    ]

    email = models.EmailField(unique=True, verbose_name='E-mail')
    region = models.CharField(max_length=50, verbose_name='Регион')
    city = models.CharField(max_length=30, null=True, blank=True, verbose_name='Город')
    first_name = models.CharField(max_length=30, blank=True, verbose_name='Имя')
    last_name = models.CharField(max_length=30, blank=True, verbose_name='Фамилия')
    patronymic = models.CharField(max_length=30, blank=True, null=True, verbose_name='Отчество')
    birthday = models.DateField(auto_now_add=False, blank=True, null=True, verbose_name='Дата рождения')
    phone = models.IntegerField(blank=True, null=True, verbose_name='Номер телефона')
    skype = models.CharField(max_length=20, blank=True, null=True, verbose_name='Skype')
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_confirm = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=True)
    avatar = models.ImageField(null=True, blank=True, upload_to=get_avatar_path)
    position = models.CharField(max_length=30, choices=DEPARTAMENT_CHOICES, default=DIRECTOR, verbose_name='Должность')
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Product(models.Model):
    # user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Польователь')
    serial_number = models.CharField(max_length=30, verbose_name='Серийный номер')
    name = models.TextField(verbose_name='Наименование')
    ingredients = models.TextField(verbose_name='Ингредиенты')
    contraindications = models.TextField(verbose_name='Противопоказания')
    valid_from = models.DateField(auto_now_add=False, null=True, blank=True)
    valid_to = models.DateField(auto_now_add=False, null=True, blank=True)

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'


class Sale(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Польователь')
    serial_number = models.CharField(max_length=30, verbose_name='Серийный номер')
    name = models.TextField(verbose_name='Наименование')
    sale_date = models.DateField(auto_now_add=False)
    quantity = models.IntegerField(verbose_name='Количество')
    price = models.IntegerField(verbose_name='Цена за ед.')
    sum = models.IntegerField(verbose_name='Сумма')

    class Meta:
        verbose_name = 'Продажа'
        verbose_name_plural = 'Продажи'


class Production(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Польователь')
    serial_number = models.CharField(max_length=30, verbose_name='Серийный номер')
    name = models.TextField(verbose_name='Наименование')
    ingredients = models.TextField(verbose_name='Ингредиенты')
    quantity = models.IntegerField(verbose_name='Количество')
    production_date = models.DateField(auto_now_add=False)

    class Meta:
        verbose_name = 'Производство'
        verbose_name_plural = 'Производства'


class Request(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Пользователь')
    text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявки'





