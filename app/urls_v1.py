from django.urls import path

from .views import (
    sign_in, sign_up,
    UserListView, UserDetailView,
    ProductListView, ProductDetailVIew,
    SaleListView, SaleDetailVIew,
    ProductionListView, ProductionDetailView,
    RequestListView, RequestDetailVIew,
)

app_name = 'app'

urlpatterns = [
    path('auth/signin/', sign_in),
    path('auth/signup/', sign_up),
    path('users/', UserListView.as_view()),
    path('users/<int:pk>/', UserDetailView.as_view()),
    path('products/', ProductListView.as_view()),
    path('products/<int:pk>/', ProductDetailVIew.as_view()),
    path('sales/', SaleListView.as_view()),
    path('sales/<int:pk>/', SaleDetailVIew.as_view()),
    path('productions/', ProductionListView.as_view()),
    path('productions/<int:pk>/', ProductionDetailView.as_view()),
    path('requests/', RequestListView.as_view()),
    path('requests/<int:pk>/', RequestDetailVIew.as_view()),
]
